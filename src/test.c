#include "test.h"
#include <stdbool.h>

void* heap = NULL;
void heap_initialize(size_t size) {
    heap = heap_init(size);
    debug_heap(heap);
    printf("Heap was initialized");
}

bool test_mem_aloc() {
    printf("\nTest1 started:\n");
    void* block = _malloc(1000);
    if (!block) {
        printf("Error in test1\n");
        return false;
    }
    debug_heap(heap);
    _free(block);
    debug_heap(heap);
    printf("Test1 passed\n");
    return true;
}

bool test_release_one_block() {
    printf("Test2 started:\n");
    void* block1 = _malloc(1000);
    void* block2 = _malloc(2000);
    if (!block1 || !block2) {
        printf("Error in test2\n");
        return false;
    }
    debug_heap(heap);
    _free(block1);
    debug_heap(heap);
    _free(block2);
    printf("Test2 passed\n");
    return true;
}


bool test_release_two_blocks() {
    printf("Test3 started\n");
    void* block1 = _malloc(1000);
    void* block2 = _malloc(1500);
    void* block3 = _malloc(3000);
    if (!block1 || !block2 || !block3) {
        printf("Error in test3\n");
        return false;
    }
    debug_heap(heap);
    _free(block1);
    debug_heap(heap);
    _free(block2);
    debug_heap(heap);
    _free(block3);
    printf("Test3 passed\n");
    return true;
}

bool test_memory_expansion_test() {
    printf("Test4 started\n");
    void* block1 = _malloc(1000);
    void* block2 = _malloc(2000);
    void* block3 = _malloc(10000);
    if (!block1 || !block2 || !block3) {
        printf("Error in test4\n");
        return false;
    }
    debug_heap(heap);
    _free(block1);
    _free(block2);
    _free(block3);
    printf("Test4 passed\n");
    return true;
}

bool test_new_region() {
    printf("Test5 started\n");
    void* block1 = _malloc(8000);
    void* block2 = _malloc(20000);
    void* block3 = _malloc(100000);
    if (!block1 || !block2 || !block3) {
        printf("Error in test5\n");
        return false;
    }
    debug_heap(heap);
    _free(block1);
    _free(block2);
    _free(block3);
    printf("Test5 passed\n");
    return true;
}
