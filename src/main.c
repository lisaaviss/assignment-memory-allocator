#include "test.h"
int main() {
    heap_initialize(10000);
    test_mem_aloc();
    test_release_one_block();
    test_release_two_blocks();
    test_memory_expansion_test();
    test_new_region();
    printf("End of the tests");
    return 0;
}
