#ifndef _TEST_H_
#define _TEST_H_

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>

void heap_initialize(size_t size);
bool test_mem_aloc();
bool test_release_one_block();
bool test_release_two_blocks();
bool test_memory_expansion_test();
bool test_new_region();


#endif
